
{
  "products": [
   {"name": "Monster Octagon Headphones",
   "imgurl":"https://images-na.ssl-images-amazon.com/images/I/41eenZCWn2L._AC_.jpg",
   "link": "https://www.amazon.com/Monster-Octagon-Over-Ear-Headphones-Matte/dp/B00M9DQOHY/ref=sr_1_11",
   "logo": "https://gitlab.com/CodeDarkin/vidarinboxes/-/raw/master/youtube/monster.jpeg",
   "code":"headphone"
   },
   {"name": "Combat Fight Gloves",
   "imgurl":"https://smhttp-ssl-72612.nexcesscdn.net/media/catalog/product/cache/7/thumbnail/9df78eab33525d08d6e5fb8d27136e95/h/b/hbg1_v1.jpg",
   "link": "https://www.combatsports.com/gloves-1/mma-gloves/mma-bag-gloves/combat-sports-mma-bag-gloves.html",
   "logo":"https://gitlab.com/CodeDarkin/vidarinboxes/-/raw/master/youtube/combatgloves.png",
   "code": "gloves"
   }
             ],
  "timeframes":
  [{"time": [9000,11000], "boxes": [["h", [9, 14, 69, 64],[20, 14, 69, 64]],
  ["headphone",[8, 14, 62, 78],[8, 34, 62, 78]]]},

  {"time": [11000, 13500], "boxes":
  [["headphone",[7,10,75,75],[7,10,75,75]],
   ["headphone",[7,10,70,82],[7,10,70,82]],
   ["headphone",[6,10,65,88],[6,10,65,88]]]},

{"time": [14500, 15600], "boxes":
  [["headphone",[4,6,44,55],[4,6,44,55]],
   ["headphone",[4,6,44,60],[4,6,44,60]]]},

{"time": [17800, 20200], "boxes":
  [["headphone",[13,15,65,64],[13,15,65,64]],
  ["headphone",[8,14,62,76],[8,14,62,76]]]},

{"time": [25400, 27920], "boxes":
  [["headphone",[13,15,64,55],[13,15,64,55]],
  ["headphone",[7,14,60,71],[7,14,60,71]]]},

{"time": [32000, 34520], "boxes":
  [["headphone",[13,15,64,60],[13,15,64,60]],
  ["headphone",[7,14,60,75],[7,14,60,75]]]},

{"time": [39800, 46220], "boxes":
  [["headphone",[13,15,64,60],[13,15,60,60]],
  ["headphone",[7,14,60,77],[7,14,58,75]]]},

{"time": [51300, 52400], "boxes":
  [["headphone",[13,15,47,55],[13,15,18,40]]]},

{"time": [52400, 53600], "boxes":
  [["headphone",[13,15,18,40],[20,15,45,55]]]},

{"time": [55500, 59500], "boxes":
  [["headphone",[18,15,65,70],[18,15,65,62]]]},

{"time": [59500, 60500], "boxes":
  [["headphone",[24,18,60,60],[24,18,60,52]]]},

{"time": [60500, 69500], "boxes":
  [["headphone",[24,18,60,52],[24,18,60,52]]]},


{"time": [69500, 71500], "boxes":
  [["headphone",[24,18,60,52],[24,18,48,35]]]},

{"time": [71500, 74600], "boxes":
  [["headphone",[22,18,42,35],[22,18,42,35]]]},

{"time": [98000, 100000], "boxes":
  [["gloves",[14,10,70,56],[18,13,65,53]]]},

{"time": [100000, 131000], "boxes":
  [["gloves",[18,13,65,53],[18,13,65,43]]]},

{"time": [131000, 141000], "boxes":
  [["gloves",[20,15,61,31],[20,15,61,31]]]},

{"time": [151000, 181000], "boxes":
  [["gloves",[25,40,9,16],[25,40,12,2]]]}

]
}
